def get_tbl_keys(app, table):
    '''
    Takes an string application name and table name and returns a list
    of strings containing the application's keys for that table.
    '''
    ap_keys = {"AHFS_CODES": "ahfs_id",
               "AP_ADU_FOR_MEDBOARD": "recid",
               "AP_ADU_PICKS_FOR_PYXIS": "id",
               "AP_ADU_PICKS_FOR_PYXIS_LOOKUP": "log_id",
               "AP_APPLICATION_MENUS": "menu_id",
               "AP_APPLICATION_MENU_RIGHTS": "app_rights_id",
               "AP_AUTH_ROLE": "role_id",
               "AP_AUTH_USER": "user_id",
               "AP_BAD_SCAN": "bad_scan_id",
               "AP_COC_INSTANCE": "process_instance_id",
               "AP_COC_INSTANCE_STAGE": "instance_stage_id",
               "AP_COC_PROCESS": "process_id",
               "AP_COC_PROCESS_STAGE": "process_stage_id",
               "AP_COC_PROCESS_STAGE_NEXT": ["next_stage_li",
                                             "prev_stage_li", "process_li"],
               "AP_CONFIGURATION_DETAIL": ["category", "code"],
               "AP_CONFIGURATION_MASTER": "category",
               "AP_DRUG_FACILITY_XREF": "drug_facility_xref",
               "AP_DRUG_IDENTIFIERS_XREF": "identifier_id",
               "AP_DRUG_LOC_EXPIRY_LOT": "lot_exp_id",
               "AP_DRUG_MFG_XREF": "mfg_id",
               "AP_DRUG_MFG_XREF_TRACK": "id",
               "AP_DRUGMASTER": "drug_id",
               "AP_DRUGMASTER_TRACK": "id",
               "AP_FACILITY_WHOLESALER_XREF": "id",
               "AP_IMPORTS": "imports_id",
               "AP_IMPORTS2": "imports_id",
               "AP_INVOICE": "id",
               "AP_INVOICE_DETAILS": "invoice_detail_id",
               "AP_LIMITED_FORMULARY": "lf_id",
               "AP_LIMITED_FORMULARY_DETAILS": "lf_detail_id",
               "AP_LOCATION": "location_type_id",
               "AP_LOCATION_DETAIL": "location_detail_id",
               "AP_LOCATION_DETAIL_TRACK": "id",
               "AP_LOCATION_VIRTUAL": "vloc_id",
               "AP_MEDBOARD_PICKS_LOOKUP": "log_id",
               "AP_MOVED_TO_ACTIVE": "m_id",
               "AP_ORDER_HISTORY": "log_id",
               "AP_PICK_DETAIL_TYPE": "pick_detail_type_id",
               "AP_PICK_DETAILS": "pick_detail_id",
               "AP_PICK_QUEUE": "pick_queue_id",
               "AP_PICK_STATUS": "ap_pick_status_id",
               "AP_PICK_TYPE": "pick_type_id",
               "AP_PICK_TYPE_FACILITY_ROUTING": "routing_id",
               "AP_PICK_TYPE_FACILITY_XREF": "pick_type_facility_xref_id",
               "AP_PRINT_QUEUE_SORT": "seq_id",
               "AP_PRINTTYPE": "id",
               "AP_PRINTTYPE_XREF": "id",
               "AP_QTY_DESCREP": "qty_descrep_id",
               "AP_QTY_DESCREP_REASONS": "reason_id",
               "AP_QUEUE_DETAILS": "queue_details_id",
               "AP_QUEUE_TOTALS": "id",
               "AP_REPLIES": "replies_id",
               "AP_REPLIES2": "replies_id",
               "AP_REPRINTS_LOG": "log_id",
               "AP_SCAN_LABEL": "scan_label_id",
               "AP_SCAN_LABEL_TYPE": "scan_label_type_id",
               "AP_STAGING_PICK_QUE": "recid",
               "AP_STAGING_PICK_QUE2": "recid",
               "AP_START_WINDOWS": "start_window_id",
               "AP_TABLE_SYNC": "table_sync_id",
               "AP_TEMP_FORMULARY": "maint_id",
               "AP_USER_FACILITY_XREF": "id",
               "AP_WHOLESALER": "wholesaler_id",
               "AP_WHOLESALER_PRI": "wholesaler_pri_id",
               "AP_XFAC_PACK_XREF": ["phys_device_li", "virt_device_li"],
               "AP_ZONE": "zone_id",
               "AP_ZONE_STATUS": "zone_status_id",
               "DEA_CLASS": "deaclass_id",
               "DM_AUTOCOOL_LOG": "log_id",
               "DM_DEVICE": "device_id",
               "DM_DEVICE_CLASS": "device_class_id",
               "DM_DEVICE_TYPE": "device_type_id",
               "DM_PRINT_LAYOUT": "layout_id",
               "DM_TRANSACTION": "trans_id",
               "DM_TRANSACTION_STEP": "id",
               "DM_TRANSACTION_STEPTYPE": "steptype_id",
               "FACILITY": "facility_id",
               "FACILITY_MAIN": "facility_main_id",
               "IDENTIFIER_TYPE": "identifiertype_id"
               }

    if app == 'autopharm':
        keys = ap_keys[table]

        keys_list = []
        if isinstance(keys, str):
            keys = keys_list.append(keys)
        elif isinstance(keys, list):
            keys_list = keys

    else:
        keys_list = 'Unknown app: {}'.format(app)

    return keys_list
