import boto3
from botocore.config import Config
import json
import os
from sqlalchemy import create_engine, MetaData, Table
from table keys import get_tbl_keys
import warnings

# this is to supress a SQLAlchemy warning regarding deprecation of a 
# module's default behavior. That is not used in this script now so 
# we are supressing warnings to avoid disruption of lambda and false
# errors/alarms
warnings.filterwarnings("ignore")


def get_s3_file(bucket_name, filename, aws_key, aws_secret):
    s3 = boto3.resource('s3',
                        aws_access_key_id=aws_key,
                        aws_secret_access_key=aws_secret,
                        config=Config(signature_version='s3v4')
                        )
    obj = s3.Object(bucket_name, filename)
    body = obj.get()['Body'].read()
    return body


def read_json_data(file):
    '''
    Takes a bytes array and returns a dict.
    '''
    file = json.loads(file.decode('utf-8'))
    return file


def extract_info(json_file):
    '''
    Takes a JSON file from consumer and returns application,
    source table, nested JSON data, and a bool for if the records
    are from an initial data load or on going cdc changes.

    Requires JSON to have keys Application, DataSet, and RowData.
    '''
    # reading in file for json
    json_data = read_json_data(json_file)
    app = ''.join(i for i in json_data['Application'] if not i.isdigit())
    data = json_data['RowData']

    cdc = False

    if json_data['DataSet'].split('.')[0] == 'cdc':
        # ugly just takes the application portion and if it is
        # cdc.dbo_TABLE_CT then strips all but TABLE
        source_table = json_data['DataSet'].split('.')[1][4:-3]
        cdc = True
    else:
        source_table = json_data['DataSet']
        cdc = False

    return app, source_table, data, cdc


def remove_cdc(row):
    for item in ('command_id', 'operation', 'seqval', 'start_lsn',
                 'update_mask', 'end_lsn'):
        del row[item]
    return row


def con_creation(flavor, host, db, port, user, password):
    '''
    Creates database URI database connection with SQLAlchemy.
    '''
    engine = create_engine('{flavor}://{user}:{password}@{host}:{port}/{db}'
                           .format(flavor=flavor, user=user,
                                   password=password, host=host,
                                   port=port, db=db))
    return engine


def lambda_handler(event, context):
    # variables for lambda instance **CUSTOMER SPECIFIC**
    host = os.environ['dbhost']
    port = os.environ['port']
    db = os.environ['dbname']
    user = os.environ['dbuser']
    password = os.environ['dbpass']

    # variables from the file put event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_name = event['Records'][0]['s3']['object']['key']
    print(file_name + ' read')

    # these may be changed or may be unecessary depedent upon
    # how IAM rules work
    aws_key = os.environ['access_key_id']
    aws_secret = os.environ['access_secret_key']

    body = get_s3_file(bucket_name, file_name, aws_key, aws_secret)
    app, source_table, data, cdc = extract_info(body)
    print('app: {app} \r\n \
        table: {tbl} \r\n cdc: {cdc}'.format(app=app,
                                             tbl=source_table,
                                             cdc=cdc))

    # creating connetion info
    engine = con_creation('postgresql', host, db, port, user, password)
    metadata = MetaData(schema=app)
    metadata.bind = engine

    # Trying to create our table object that we will connect to.
    try:
        table = Table(source_table, metadata, autoload=True)
        print('table object created')
    except:
        print('Table {} does not exist and must be created prior to\
            loading data.'.format(source_table))

    tbl_keys = get_tbl_keys(app, source_table)

    if cdc:
        for row in data:
            if row['operation'] == '1':
                # delete
                print('operation: 1')
                keys = []
                for key in tbl_keys:
                    keys.append('{key_val} = {key}'.format(
                        key_val=row[key], key=key))
                where = ', '.join(keys)
                del_stmt = table.delete()
                del_stmt = del_stmt.where(where)
                conn = engine.connect()
                result = conn.execute(del_stmt)

            elif row['operation'] == '2':
                # insert
                print('operation: 2')
                clean_row = remove_cdc(row)
                ins = table.insert().values(clean_row)
                conn = engine.connect()
                result = conn.execute(ins)

            elif row['operation'] == '4':
                print('operation: 4')
                # update final
                tbl_key = get_tbl_keys(app, source_table)
                keys = []
                for key in tbl_keys:
                    keys.append('{key_val} = {key}'.format(
                        key_val=row[key], key=key))
                where = ', '.join(keys)
                clean_row = remove_cdc(row)
                update_stmt = table.update().values(
                                             clean_row).where(where)
                conn = engine.connect()
                result = conn.execute(update_stmt)

            elif row['operation'] == '3':
                # update prior value so don't do anything ? I guess, yeah.
                pass

    elif not cdc:
        print('Data set contains {} records'.format(len(data)))
        conn = engine.connect()
        result = conn.execute(table.insert(), data)
        return 'Inserted {} rows'.format(str(result.rowcount))
